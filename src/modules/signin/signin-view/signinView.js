import React, {  useState } from 'react';
import {
  View,
  ActivityIndicator
} from 'react-native';
import LoginScreen from "react-native-login-screen";
import firebase from 'react-native-firebase';
import { styles } from './styles';

const SignInViewContainer = ({ navigation }) => {
    const [loading, setLoader] = useState(false);
    const [email, setEmail] = useState(false);
    const [password, setPassword] = useState(false);

    const submit = async () => {
        setLoader(true);
        const response = await firebase.auth().signInWithEmailAndPassword(email,password);
        if (response !== undefined || response !== null) {
            navigation.navigate('Home');
        }
        setLoader(false);
    }

    if (loading) {
        return ( 
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} size="large" color="red" />
            </View>
        );
    }

    return (
        <View style={styles.container}>
            <LoginScreen
                source={{
                    uri: "https://images.unsplash.com/photo-1513730775149-a1d05906dea0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
                }}
                onPressLogin={() => submit()}
                onPressSettings={() => alert("Settings Button is pressed")}
                switchValue={true}
                logoText="GRINGU"
                onSwitchValueChange={switchValue => alert(switchValue) }
                usernameOnChangeText={username => setEmail(username)}
                passwordOnChangeText={password => setPassword(password)}
                loginButtonBackgroundColor="#a2a5a9"  
            />
        </View>
    );
};

export { SignInViewContainer };
