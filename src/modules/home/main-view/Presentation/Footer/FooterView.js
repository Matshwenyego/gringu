import React from 'react';
import {
  View,
  Text,
  TouchableOpacity, 
} from 'react-native';
import { styles } from './styles';

const FooterView = ({ onAdd }) => {
    return (
        <View style={{ flex: 0.05, margin: '2%' }}>
            <TouchableOpacity onPress={onAdd} style={{ borderRadius: 4, height: '140%', borderWidth: 1, backgroundColor: 'black', justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: '#FFFFFF' }}>ADD</Text>
            </TouchableOpacity>
        </View>
    );
};

export { FooterView };
