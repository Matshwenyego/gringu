import { HeaderView } from './Header';
import { BodyView } from './Body';
import { FooterView } from './Footer';

export { 
    HeaderView,
    BodyView, 
    FooterView,
};