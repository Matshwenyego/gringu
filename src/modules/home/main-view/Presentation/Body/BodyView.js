import React from 'react';
import {
  View,
  Text,
  TouchableOpacity, 
  FlatList
} from 'react-native';
import { styles } from './styles';

const BodyView = ({ data }) => {
    return (
        <View style={styles.container}>
            <FlatList
                scrollEnabled
                style={styles.list}
                data={data}
                renderItem={({item}) => (
                    <TouchableOpacity onPress={() => null} style={styles.card}>
                        <View style={styles.rightside}>
                            <Text style={styles.text}>{`Patient name: ${item.name}`}</Text>
                            <Text style={styles.text}>{`File #: ${item.fileNo}`}</Text>
                        </View>
                        <View style={styles.leftside}>
                            <Text>{`Doctor: ${item.doctor}`}</Text>
                            <Text>{`Clinic: ${item.practiceName}`}</Text>
                        </View>
                    </TouchableOpacity>
                )}
                keyExtractor={item => item.id}
            />
        </View>
    );
};

export { BodyView };
