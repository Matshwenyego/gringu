import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: { 
        flex: 0.9, 
        margin: '2%', 
    },
    list: { 
        flex: 1, 
    },
    card: { 
        flex: 1, 
        flexDirection: 'row', 
        borderWidth: 1, 
        borderRadius: 4, 
        marginBottom: 10, 
    },
    text: { 
        textAlign: 'left', 
    },
    rightside: { 
        flex: 0.5, 
        marginLeft: 20, 
    },
    leftside: { 
        flex: 0.5, 
        justifyContent: 'flex-end', 
        alignItems: 'flex-end', 
        marginRight: 20, 
    },
    
});

export { styles };