import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: { 
        flex: 0.05, 
        backgroundColor: 'black', 
        justifyContent: 'center', 
        alignItems: 'center', 
    },
    title: { 
        color: '#FFFFFF' 
    },
});

export { styles };