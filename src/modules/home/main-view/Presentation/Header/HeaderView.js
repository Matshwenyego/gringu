import React from 'react';
import {
  View,
  Text,
} from 'react-native';
import { styles } from './styles';

const HeaderView = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>HOME</Text>
        </View>
    );
};

export { HeaderView };
