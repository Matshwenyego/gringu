import React from 'react';
import {
  View,
} from 'react-native';
import firebase from 'react-native-firebase';
import { HeaderView, BodyView, FooterView } from './Presentation';
import { styles } from './styles';

const data = [
    {
        id: 0,
        name: 'Ashok',
        fileNo: '77123',
        doctor: 'Ashley',
        practiceName: 'Medi clinic'
    },
    {
        id: 1,
        name: 'Ashok',
        fileNo: '41237',
        doctor: 'Obah',
        practiceName: 'Netcare'
    },
]

const readData = async () => {
    try{
        const ref = firebase.database().ref('clinics/');
        console.log('ref: ', ref);
        const snapshot = await ref.once('value');
        console.log('snapshot: ', snapshot);
    }catch(error){
        console.log('error: ', error);
    }
    // console.log('User data: ', snapshot.val());
    // console.log('firebase: ', firebase.database());
    // firebase.database().ref('clinics/').once('value', function (snapshot) {
    //     console.log(snapshot.val())
    // });
}

const MainViewContainer = () => {
    return (
        <View style={styles.container}>
            <HeaderView />
            <BodyView data={data} />
            <FooterView onAdd={() => readData()} />
        </View>
    );
};

export { MainViewContainer };
