import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import { 
    SignInViewContainer,
    MainViewContainer,
} from '../modules';

const AuthenticationStack = createStackNavigator({
    SignIn: {
        screen: SignInViewContainer,
    },
},{
    mode: 'modal',
    navigationOptions: {
        gesturesEnabled: false,
        tabBarVisible: false,
    },
    headerMode: 'none',
});

const HomeStack = createStackNavigator({
    Home: {
        screen: MainViewContainer,
    },
},{
    mode: 'modal',
    navigationOptions: {
        gesturesEnabled: false,
        tabBarVisible: false,
    },
    headerMode: 'none',
});

const AuthNav = createSwitchNavigator({
    Auth: AuthenticationStack,
    Home: HomeStack,
});

const InAppNav = createSwitchNavigator({
	Home: HomeStack,
});

const AuthNavigation = createAppContainer(AuthNav);
const MainNavigation = createAppContainer(InAppNav);

export { AuthNavigation, MainNavigation };

