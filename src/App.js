import React from 'react';
import {
  SafeAreaView,
  View,
  StatusBar,
} from 'react-native';
import 'react-native-gesture-handler';
import { AuthNavigation } from './navigation/router';
import { styles } from './styles';

const App = () => {
  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
        <AuthNavigation/>
      </SafeAreaView>
    </View>
  );
};

export default App;
